module Msgs exposing (..)

import AedInfo exposing (..)


type Msg
    = Select
    | AEDNtrxn
    | MechOfAct
    | WarfNtrxn
    | OCPNtrxn
    | PregnancyData
    | Selected AedInfo.Drug
    | Unselected AedInfo.Drug
    | ClearDrugs
    | NoOp
