module Colors exposing (..)

import Element exposing (..)


type Palette
    = Primary
    | PrimaryDark
    | PrimaryLight
    | Secondary
    | SecondaryDark
    | SecondaryLight
    | Tertiary
    | TertiaryDark
    | TertiaryLight
    | White
    | Black


color : Palette -> Color
color palettechoice =
    case palettechoice of
        Primary ->
            rgba255 210 129 125 1

        --204 146 143 1
        --
        --202 82 76 1
        PrimaryDark ->
            rgba255 153 73 69 1

        PrimaryLight ->
            rgba255 230 116 110 1

        Secondary ->
            rgba255 147 175 210 1

        --125 163 210 1
        --210 210 90 1
        SecondaryDark ->
            rgba255 100 131 168 1

        --189 189 81 1
        SecondaryLight ->
            rgba255 150 195 252 1

        --231 231 99 1
        Tertiary ->
            rgba255 143 204 176 1

        --125 210 171 1
        --63 75 138 1
        TertiaryLight ->
            rgba255 150 252 205 1

        --69 83 152 1
        TertiaryDark ->
            rgba255 125 179 147 1

        --100 189 137 1
        --57 67 125 1
        White ->
            rgba255 230 235 240 1

        Black ->
            rgba255 51 51 51 1



--20 25 30 1
