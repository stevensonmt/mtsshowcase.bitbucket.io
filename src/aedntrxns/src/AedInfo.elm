module AedInfo exposing (..)


type Drug
    = Carbamazepine
    | Clobazam
    | Clonazepam
    | Eslicarbazepine
    | Ethosuximide
    | Felbamate
    | Gabapentin
    | Lacosamide
    | Lamotrigine
    | Levetiracetam
    | Oxcarbazepine
    | Perampanel
    | Phenobarbital
    | Phenytoin
    | Pregabalin
    | Primidone
    | Retigabine
    | Rufinamide
    | Stiripentol
    | Tiagabine
    | Topiramate
    | Valproate
    | Vigabatrin
    | Zonisamide


aedinfo drug =
    case drug of
        Zonisamide ->
            zonisamide

        Vigabatrin ->
            vigabatrin

        Valproate ->
            valproate

        Topiramate ->
            topiramate

        Tiagabine ->
            tiagabine

        Stiripentol ->
            stiripentol

        Rufinamide ->
            rufinamide

        Retigabine ->
            retigabine

        Primidone ->
            primidone

        Pregabalin ->
            pregabalin

        Phenytoin ->
            phenytoin

        Phenobarbital ->
            phenobarbital

        Perampanel ->
            perampanel

        Oxcarbazepine ->
            oxcarbazepine

        Levetiracetam ->
            levetiracetam

        Lamotrigine ->
            lamotrigine

        Lacosamide ->
            lacosamide

        Gabapentin ->
            gabapentin

        Felbamate ->
            felbamate

        Ethosuximide ->
            ethosuximide

        Eslicarbazepine ->
            eslicarbazepine

        Clonazepam ->
            clonazepam

        Clobazam ->
            clobazam

        Carbamazepine ->
            carbamazepine


type MechanismOfAction
    = NaChannel
    | HVACaChannel
    | LVACaChannel
    | KChannel
    | GABAaReceptor
    | GABATurnover
    | GlutReceptor
    | SVP2A
    | CarbonicAnhydrase


type Prominence
    = OnePlus
    | TwoPlus
    | ThreePlus


type WarfarinInteraction
    = DecrINR
    | DoseDepDecrINR
    | IncrINR
    | Unpredictable
    | NoWarfInteraction


warfToString wrfntrxn =
    case wrfntrxn of
        DecrINR ->
            "Increases metabolism of warfarin leading to decrease in INR."

        DoseDepDecrINR ->
            "Dose-dependent effect on warfarin metabolislm. Doses > 200mg/day may lower INR."

        IncrINR ->
            "Increase in INR due to inhibition of warfarin metabolism, displacement of warfarin from protein binding, or both."

        Unpredictable ->
            "Complex and unpredictable effects on warfarin metabolism. Avoid this combination when possible."

        NoWarfInteraction ->
            "Low interaction potential with warfarin."


type OCPFX
    = IncrMetab
    | DoseDepIncrMetab
    | ModestReductionProgestin
    | ReductionProgestin
    | NoOCPInteraction


ocpfxToString ocpfx =
    case ocpfx of
        IncrMetab ->
            "Increases metabolism of hormonal contraceptions, potentially decreasing contraceptive efficacy."

        DoseDepIncrMetab ->
            "No effect at doses < 100mg/day. Modest effect at dose of 200mg/day. Consistently increases metabolism of hormonal contraceptives at doses > 200mg/day, potentially decreasing contraceptive efficacy."

        ModestReductionProgestin ->
            "Modest reduction in progestin levels at doses " ++ (String.fromChar ('≥')) ++ " 300mg/day."

        ReductionProgestin ->
            "Reduction in progestin levels at doses " ++ (String.fromChar ('≥')) ++ " 12mg/day."

        NoOCPInteraction ->
            "No significant effect on hormonal contraception metabolism."


type alias CongenitalMalformationRate =
    Maybe Float


pregToString preg =
    case preg of
        Nothing ->
            "Insufficient or no data reported."

        Just x ->
            "North American AED Pregnancy Registry reported rate of major congenital malformations: " ++ (String.left 3 (Debug.toString (x * 100))) ++ "%"


type alias AEDinfo =
    { mechanism : List ( MechanismOfAction, Prominence )
    , warfarin : WarfarinInteraction
    , ocpfx : OCPFX
    , preg : CongenitalMalformationRate
    }


carbamazepine =
    { mechanism = [ ( NaChannel, ThreePlus ) ], warfarin = DecrINR, ocpfx = IncrMetab, preg = Just 0.03 }


clobazam =
    { mechanism = [ ( GABAaReceptor, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


clonazepam =
    { mechanism = [ ( GABAaReceptor, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Just 0.031 }


eslicarbazepine =
    { mechanism = [ ( NaChannel, ThreePlus ) ], warfarin = DecrINR, ocpfx = IncrMetab, preg = Nothing }


ethosuximide =
    { mechanism = [ ( LVACaChannel, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


felbamate =
    { mechanism = [ ( NaChannel, TwoPlus ), ( HVACaChannel, TwoPlus ), ( GABAaReceptor, TwoPlus ), ( GlutReceptor, TwoPlus ) ], warfarin = NoWarfInteraction, ocpfx = IncrMetab, preg = Nothing }


gabapentin =
    { mechanism = [ ( NaChannel, OnePlus ), ( HVACaChannel, TwoPlus ), ( GABATurnover, OnePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Just 0.007 }


lacosamide =
    { mechanism = [ ( NaChannel, ThreePlus ), ( CarbonicAnhydrase, OnePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


lamotrigine =
    { mechanism = [ ( NaChannel, ThreePlus ), ( HVACaChannel, TwoPlus ) ], warfarin = NoWarfInteraction, ocpfx = ModestReductionProgestin, preg = Just 0.02 }


levetiracetam =
    { mechanism = [ ( HVACaChannel, OnePlus ), ( GABAaReceptor, OnePlus ), ( SVP2A, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Just 0.024 }


oxcarbazepine =
    { mechanism = [ ( NaChannel, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = IncrMetab, preg = Just 0.022 }


perampanel =
    { mechanism = [ ( GlutReceptor, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = ReductionProgestin, preg = Nothing }


phenobarbital =
    { mechanism = [ ( HVACaChannel, OnePlus ), ( GABAaReceptor, ThreePlus ), ( GlutReceptor, OnePlus ) ], warfarin = DecrINR, ocpfx = IncrMetab, preg = Just 0.055 }


phenytoin =
    { mechanism = [ ( NaChannel, ThreePlus ) ], warfarin = Unpredictable, ocpfx = IncrMetab, preg = Just 0.029 }


pregabalin =
    { mechanism = [ ( HVACaChannel, TwoPlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


primidone =
    { mechanism = [ ( HVACaChannel, OnePlus ), ( GABAaReceptor, ThreePlus ), ( GlutReceptor, OnePlus ) ], warfarin = DecrINR, ocpfx = IncrMetab, preg = Nothing }


retigabine =
    { mechanism = [ ( KChannel, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


rufinamide =
    { mechanism = [ ( NaChannel, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = IncrMetab, preg = Nothing }


stiripentol =
    { mechanism = [ ( GABAaReceptor, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


tiagabine =
    { mechanism = [ ( GABATurnover, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


topiramate =
    { mechanism = [ ( NaChannel, TwoPlus ), ( HVACaChannel, TwoPlus ), ( KChannel, OnePlus ), ( GABAaReceptor, TwoPlus ), ( GlutReceptor, TwoPlus ), ( CarbonicAnhydrase, OnePlus ) ], warfarin = DoseDepDecrINR, ocpfx = DoseDepIncrMetab, preg = Just 0.042 }


valproate =
    { mechanism = [ ( NaChannel, TwoPlus ), ( LVACaChannel, TwoPlus ), ( GABATurnover, TwoPlus ) ], warfarin = IncrINR, ocpfx = NoOCPInteraction, preg = Just 0.093 }


vigabatrin =
    { mechanism = [ ( GABATurnover, ThreePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Nothing }


zonisamide =
    { mechanism = [ ( NaChannel, ThreePlus ), ( LVACaChannel, TwoPlus ), ( CarbonicAnhydrase, OnePlus ) ], warfarin = NoWarfInteraction, ocpfx = NoOCPInteraction, preg = Just 0.0 }


combination : Drug -> Drug -> String
combination drug1 drug2 =
    case drug1 of
        Carbamazepine ->
            (case drug2 of
                Clobazam ->
                    changes ( Clobazam, 4 ) ( Carbamazepine, 0 )

                Clonazepam ->
                    changes ( Clonazepam, 4 ) ( Carbamazepine, 0 )

                Eslicarbazepine ->
                    changes ( Eslicarbazepine, 3 ) ( Carbamazepine, 0 )

                Ethosuximide ->
                    changes ( Ethosuximide, 4 ) ( Carbamazepine, 0 )

                Felbamate ->
                    changes ( Felbamate, 4 ) ( Carbamazepine, 3 )

                Gabapentin ->
                    changes ( Gabapentin, 0 ) ( Carbamazepine, 0 )

                Lacosamide ->
                    changes ( Lacosamide, 0 ) ( Carbamazepine, 0 )

                Lamotrigine ->
                    changes ( Lamotrigine, 4 ) ( Carbamazepine, 0 )

                Levetiracetam ->
                    changes ( Levetiracetam, 0 ) ( Carbamazepine, 0 )

                Oxcarbazepine ->
                    changes ( Oxcarbazepine, 3 ) ( Carbamazepine, 3 )

                Perampanel ->
                    changes ( Perampanel, 4 ) ( Carbamazepine, 3 )

                Phenobarbital ->
                    changes ( Phenobarbital, 0 ) ( Carbamazepine, 4 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( Phenytoin, 2 ) ( Carbamazepine, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( Pregabalin, 0 ) ( Carbamazepine, 0 )

                Primidone ->
                    changes ( Primidone, 3 ) ( Carbamazepine, 4 )

                Retigabine ->
                    changes ( Retigabine, 3 ) ( Carbamazepine, 0 )

                Rufinamide ->
                    changes ( Rufinamide, 3 ) ( Carbamazepine, 3 )

                Stiripentol ->
                    changes ( Stiripentol, 4 ) ( Carbamazepine, 2 )

                Tiagabine ->
                    changes ( Tiagabine, 4 ) ( Carbamazepine, 0 )

                Topiramate ->
                    changes ( Topiramate, 4 ) ( Carbamazepine, 0 )

                Valproate ->
                    changes ( Valproate, 4 ) ( Carbamazepine, 2 )

                Vigabatrin ->
                    changes ( Vigabatrin, 0 ) ( Carbamazepine, 1 )

                Zonisamide ->
                    changes ( Zonisamide, 4 ) ( Carbamazepine, 0 )

                _ ->
                    "That's an error!"
            )
                ++ "\n"
                ++ autoinduction Carbamazepine

        Clobazam ->
            case drug2 of
                Clobazam ->
                    "That's an error!"

                Clonazepam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Eslicarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Ethosuximide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Felbamate ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ " While felbamate causes significant decline in clobazam levels, the levels of the active metabolite N-desmethylcobazam may significantly increase."

                Gabapentin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 2 ) ++ " The active metabolite of clobazam N-desmethylcobazam also significantly increases." ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 1 ) ( drug1, 4 ) ++ " Effects of clobazam on phenytoin metabolism can be inconsistent and may cause clinically significant increases in phenytoin levels. While phenytoin may lower clobazam levels, the levels of the active metabolite N-desmethylclobazam can significantly increase." ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 1 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 1 ) ( drug1, 2 ) ++ " The active metabolite of clobazam N-desmethylclobazam also significantly increases."

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 1 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Clonazepam ->
            case drug2 of
                Clonazepam ->
                    "That's an error!"

                Eslicarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Ethosuximide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Felbamate ->
                    changes ( drug2, 0 ) ( drug1, 1 )

                Gabapentin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 1 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 4 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Eslicarbazepine ->
            case drug2 of
                Eslicarbazepine ->
                    "That's an error."

                Ethosuximide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Felbamate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Gabapentin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 0 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 1 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 3 ) ( drug1, 3 )

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Ethosuximide ->
            case drug2 of
                Ethosuximide ->
                    "That's an error!"

                Felbamate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Gabapentin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 1 ) ( drug1, 4 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 1 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 0 ) ++ "Effects of valproate on ethosuximide levels may be unpredictable."

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Felbamate ->
            case drug2 of
                Felbamate ->
                    "That's an error!"

                Gabapentin ->
                    changes ( drug2, 0 ) ( drug1, 1 )

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 1 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 2 ) ( drug1, 0 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 2 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 2 ) ( drug1, 1 )

                Vigabatrin ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Gabapentin ->
            case drug2 of
                Gabapentin ->
                    "That's an error!"

                Lacosamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Lamotrigine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 0 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 0 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Lacosamide ->
            case drug2 of
                Lacosamide ->
                    "That's an error!"

                Lamotrigine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Levetiracetam ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Lamotrigine ->
            case drug2 of
                Lamotrigine ->
                    "That's an error!"

                Levetiracetam ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 4 )

                Retigabine ->
                    changes ( drug2, 1 ) ( drug1, 3 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 2 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Levetiracetam ->
            case drug2 of
                Levetiracetam ->
                    "That's an error!"

                Oxcarbazepine ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Perampanel ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Oxcarbazepine ->
            case drug2 of
                Oxcarbazepine ->
                    "That's an error!"

                Perampanel ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Phenobarbital ->
                    changes ( drug2, 1 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 1 ) ( drug1, 3 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Perampanel ->
            case drug2 of
                Perampanel ->
                    "That's an error!"

                Phenobarbital ->
                    changes ( drug2, 0 ) ( drug1, 0 ) ++ "\n" ++ autoinduction Phenobarbital

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 4 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 4 )

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Phenobarbital ->
            (case drug2 of
                Phenobarbital ->
                    "That's an error!"

                Phenytoin ->
                    changes ( drug2, 0 ) ( drug1, 1 ) ++ "\n" ++ autoinduction Phenytoin

                Pregabalin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 1 ) ( drug1, 1 )

                Rufinamide ->
                    changes ( drug2, 3 ) ( drug1, 1 )

                Stiripentol ->
                    changes ( drug2, 4 ) ( drug1, 2 )

                Tiagabine ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 4 ) ( drug1, 2 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1
            )
                ++ "\n"
                ++ autoinduction Phenobarbital

        Phenytoin ->
            (case drug2 of
                Phenytoin ->
                    "That's an error!"

                Pregabalin ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Primidone ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 3 ) ( drug1, 1 )

                Stiripentol ->
                    changes ( drug2, 4 ) ( drug1, 2 )

                Tiagabine ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 4 ) ( drug1, 1 )

                Valproate ->
                    changes ( drug2, 4 ) ( drug1, 3 ) ++ "While valproate may cause the total phenytoin level to decrease, the free (pharmacologically active) level may actually increase due to effects on protein binding.  Following free levels is recommended."

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Zonisamide ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1
            )
                ++ "\n"
                ++ autoinduction Phenytoin

        Pregabalin ->
            case drug2 of
                Pregabalin ->
                    "That's an error!"

                Primidone ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Primidone ->
            case drug2 of
                Primidone ->
                    "That's an error!"

                Retigabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Rufinamide ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 4 ) ( drug1, 2 )

                Tiagabine ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 4 ) ( drug1, 2 ) ++ "The increase in primidone noted is actually an increase in the active metabolite, phenobarbital."

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 4 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Retigabine ->
            case drug2 of
                Retigabine ->
                    "That's an error!"

                Rufinamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Rufinamide ->
            case drug2 of
                Rufinamide ->
                    "That's an error!"

                Stiripentol ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 0 ) ( drug1, 1 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 3 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Stiripentol ->
            case drug2 of
                Stiripentol ->
                    "That's an error."

                Tiagabine ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 2 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Tiagabine ->
            case drug2 of
                Tiagabine ->
                    "That's an error!"

                Topiramate ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Topiramate ->
            case drug2 of
                Topiramate ->
                    "That's an error!"

                Valproate ->
                    changes ( drug2, 3 ) ( drug1, 0 )

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Valproate ->
            case drug2 of
                Valproate ->
                    "That's an error!"

                Vigabatrin ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Vigabatrin ->
            case drug2 of
                Vigabatrin ->
                    "That's an error!"

                Zonisamide ->
                    changes ( drug2, 0 ) ( drug1, 0 )

                _ ->
                    combination drug2 drug1

        Zonisamide ->
            case drug2 of
                Zonisamide ->
                    "That's an error!"

                _ ->
                    combination drug2 drug1


change drug level =
    let
        effect =
            case level of
                0 ->
                    "No significant change in "

                1 ->
                    "Minor increase in "

                2 ->
                    "Significant increase in "

                3 ->
                    "Minor decrease in "

                4 ->
                    "Significant decrease in "

                _ ->
                    "That's an error!"
    in
        effect ++ Debug.toString drug ++ " levels."


changes : ( Drug, Int ) -> ( Drug, Int ) -> String
changes ( drug2, x ) ( drug1, y ) =
    let
        effect1 =
            change drug1 x

        effect2 =
            change drug2 y
    in
        effect1 ++ "\n" ++ effect2


autoinduction drug =
    Debug.toString drug ++ " also has the effect of auto-induction of its own metabolism."
