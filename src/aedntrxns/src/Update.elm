module Update exposing (..)

import Models exposing (Model, Topic)
import Msgs exposing (Msg(..))


update : Msg -> Model -> Model
update msg model =
    case msg of
        AEDNtrxn ->
            { model | topic = Models.AEDNtrxn }

        WarfNtrxn ->
            { model | topic = Models.WarfNtrxn }

        OCPNtrxn ->
            { model | topic = Models.OCPNtrxn }

        PregnancyData ->
            { model | topic = Models.PregnancyData }

        Selected drug ->
            { model | drugs = List.append model.drugs [ drug ] }

        Unselected drug ->
            { model | drugs = List.filter (\a -> a /= drug) model.drugs }

        ClearDrugs ->
            { model | drugs = [], topic = Models.None }

        MechOfAct ->
            { model | topic = Models.MechOfAct }

        _ ->
            model
