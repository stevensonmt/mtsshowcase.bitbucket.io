module Models exposing (..)

import AedInfo exposing (..)
import List.Extra exposing (subsequences, unique)


type alias Model =
    { drugs : List Drug
    , topic : Topic
    }


type Topic
    = AEDNtrxn
    | MechOfAct
    | WarfNtrxn
    | OCPNtrxn
    | PregnancyData
    | None


drugPairs model =
    removeDuplicatePairs (lengthTwoSubsequence model.drugs)


lengthTwoSubsequence : List a -> List ( a, a )
lengthTwoSubsequence list =
    subsequences (List.sortWith compareDrugs list)
        |> List.filterMap
            (\l ->
                case l of
                    [ a, b ] ->
                        case a == b of
                            True ->
                                Nothing

                            _ ->
                                Just ( a, b )

                    _ ->
                        Nothing
            )


compareDrugs x y =
    compare (Debug.toString x) (Debug.toString y)


removeDuplicatePairs list =
    List.Extra.uniqueBy Debug.toString list
