module View exposing (..)

import Browser
import Html exposing (..)


{--import Html.Attributes exposing (..)--}
{--import Html.Events exposing (onInput)--}

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Models exposing (Model)
import Msgs exposing (Msg(..))
import Colors exposing (..)
import AedInfo exposing (..)
import Fonts exposing (..)


view : Model -> Html Msg
view model =
    baseView <|
        column [ width (px 480), centerX, spacing 20 ] <|
            List.append
                (titleBox "AEDNtrxns v3.0")
            <|
                case model.topic of
                    Models.AEDNtrxn ->
                        [ aedntrxnView model ]

                    Models.None ->
                        [ selectView model ]

                    Models.MechOfAct ->
                        [ mechanismView model ]

                    Models.WarfNtrxn ->
                        [ warfntrxnView model ]

                    Models.OCPNtrxn ->
                        [ ocpfxView model ]

                    Models.PregnancyData ->
                        [ pregDataView model ]


baseView =
    Element.layout
        [ Background.color (color Secondary)
        , Border.color (color Black)
        , Border.width 6
        , padding 10
        ]


titleBox title =
    [ row
        [ width fill
        , padding 20
        , centerX
        , Font.family [ Fonts.fontStack Headline ]
        , Font.size 42
        , Font.color (color Black)
        , Font.heavy
        , Border.color (color Black)
        , Border.width 6
        , Border.rounded 8
        , Background.color (color Tertiary)
        ]
        [ el [ centerX ] (Element.text title) ]
    ]


cardBasic content buttons =
    column [ spacing 20, width (px 480) ]
        [ (column
            [ centerX
            , width fill
            , height fill
            , Background.color (color White)
            , Border.color (color Black)
            , Border.width 6
            , Border.rounded 8
            , padding 10
            , Font.color (color Black)
            , Font.size 18
            , Font.family [ Fonts.fontStack PrimarySans ]
            ]
            content
          )
        , buttons
        ]


cardTitleBox title =
    row
        [ width (px 360)
        , Border.color (color Black)
        , Background.color (color Secondary)
        , Font.color (color Black)
        , Border.width 6
        , Border.rounded 8
        , Font.family [ Fonts.fontStack Headline ]
        , Font.heavy
        , Font.size 28
        , centerX
        , padding 10
        , spacing 10
        ]
        [ el [ centerX ] (Element.text title) ]


basicButton labelText msg alfa =
    Input.button
        [ Border.color (color Black)
        , Border.width 6
        , Border.rounded 8
        , Background.color (color TertiaryDark)
        , width (px 200)
        , padding 10
        , spacing 10
        , centerX
        , Font.family [ Fonts.fontStack PrimarySans ]
        , Font.color (color White)
        , Font.heavy
        , Font.size 18
        , alpha alfa
        ]
        { onPress = msg
        , label = Element.text labelText
        }


includeAEDNtrxn model =
    List.length model.drugs > 1


navButtons model =
    let
        inactiveTopics =
            case model.topic of
                Models.AEDNtrxn ->
                    [ Msgs.MechOfAct, Msgs.WarfNtrxn, Msgs.OCPNtrxn, Msgs.PregnancyData ]

                Models.MechOfAct ->
                    [ Msgs.AEDNtrxn, Msgs.WarfNtrxn, Msgs.OCPNtrxn, Msgs.PregnancyData ]

                Models.WarfNtrxn ->
                    [ Msgs.AEDNtrxn, Msgs.MechOfAct, Msgs.OCPNtrxn, Msgs.PregnancyData ]

                Models.OCPNtrxn ->
                    [ Msgs.AEDNtrxn, Msgs.MechOfAct, Msgs.WarfNtrxn, Msgs.PregnancyData ]

                Models.PregnancyData ->
                    [ Msgs.AEDNtrxn, Msgs.MechOfAct, Msgs.WarfNtrxn, Msgs.OCPNtrxn ]

                _ ->
                    []
    in
        column [ width (px 480), centerX, spacing 10 ]
            (List.append
                [ wrappedRow
                    [ width (px 480)
                    , height (px 116)
                    , paddingEach { top = 0, right = 24, bottom = 0, left = 40 }
                    , spacingXY 8 8
                    , centerX
                    ]
                    (List.foldr
                        (++)
                        []
                        [ List.map (topicButton model) inactiveTopics ]
                    )
                ]
                [ row [ width (px 480), centerX ]
                    [ (Input.button
                        [ padding 10
                        , centerX
                        , Background.color (color TertiaryDark)
                        , Border.color (color Black)
                        , Border.width 6
                        , Border.rounded 8
                        , width (px 200)
                        , Font.color (color White)
                        , Font.family [ Fonts.fontStack PrimarySans ]
                        , Font.heavy
                        , Font.size 18
                        ]
                        { onPress = Just ClearDrugs
                        , label = Element.text "Check Other Drugs"
                        }
                      )
                    ]
                ]
            )


topicButton model topic =
    let
        topicString =
            case topic of
                AEDNtrxn ->
                    "AED Interactions"

                MechOfAct ->
                    "Mechanism of Action"

                WarfNtrxn ->
                    "Warfarin Interactions"

                OCPNtrxn ->
                    "OCP Effects"

                PregnancyData ->
                    "Pregnancy Data"

                _ ->
                    ""
    in
        case ( topic, includeAEDNtrxn model ) of
            ( AEDNtrxn, False ) ->
                basicButton topicString Nothing 0.6

            _ ->
                basicButton topicString (Just topic) 1.0


submitButton model =
    case List.length model.drugs of
        1 ->
            basicButton "Submit" (Just MechOfAct) 1.0

        0 ->
            basicButton "Submit" Nothing 0.5

        _ ->
            basicButton "Submit" (Just AEDNtrxn) 1.0


selectView model =
    cardBasic
        [ (cardTitleBox "Select Drugs")
        , row [ centerX, spaceEvenly, paddingXY 20 16 ]
            [ (column
                [ width (px 160)
                , spacing 8
                ]
               <|
                (List.map (drugChoice model)
                    [ Carbamazepine
                    , Clobazam
                    , Clonazepam
                    , Eslicarbazepine
                    , Ethosuximide
                    , Felbamate
                    , Gabapentin
                    , Lacosamide
                    ]
                )
              )
            , (column
                [ width (px 160)
                , spacing 8
                ]
               <|
                (List.map (drugChoice model)
                    [ Lamotrigine
                    , Levetiracetam
                    , Oxcarbazepine
                    , Perampanel
                    , Phenobarbital
                    , Phenytoin
                    , Pregabalin
                    , Primidone
                    ]
                )
              )
            , (column
                [ width (px 160)
                , spacing 8
                ]
               <|
                (List.map (drugChoice model)
                    [ Retigabine
                    , Rufinamide
                    , Stiripentol
                    , Tiagabine
                    , Topiramate
                    , Valproate
                    , Vigabatrin
                    , Zonisamide
                    ]
                )
              )
            ]
        ]
    <|
        (submitButton model)


drugChoice model drug =
    case (List.member drug model.drugs) of
        True ->
            Input.button [ Background.color (color Tertiary) ]
                { onPress = Just (Unselected drug)
                , label = Element.text (Debug.toString drug)
                }

        False ->
            Input.button []
                { onPress = Just (Selected drug)
                , label = Element.text (Debug.toString drug)
                }


aedntrxnView model =
    let
        pairs =
            Models.drugPairs model
    in
        cardBasic
            (List.append
                [ (cardTitleBox "AED Interactions")
                ]
             <|
                (List.map
                    (\( x, y ) ->
                        (column [ width (px 340), centerX, spacing 8, padding 10 ]
                            [ el
                                [ Font.color (color SecondaryDark)
                                , Font.size 20
                                , Font.family [ Fonts.fontStack Headline ]
                                , Font.heavy
                                , centerX
                                ]
                                (Element.text ((Debug.toString x) ++ " and " ++ (Debug.toString y)))
                            , textColumn
                                [ Font.size 18
                                , Font.color (color Black)
                                , Font.family [ Fonts.fontStack PrimarySans ]
                                , alignLeft
                                , width (px 340)
                                ]
                                (List.map (\line -> paragraph [] [ Element.text line ]) (String.lines (AedInfo.combination x y)))
                            ]
                        )
                    )
                    pairs
                )
            )
        <|
            (navButtons model)


mechanismView model =
    cardBasic
        [ (cardTitleBox "Mechanisms of Action")
        , column
            [ centerX
            , width (px 320)
            , paddingXY 0 10
            , Font.size 20
            ]
            (List.foldr (++) [] (List.map mechOfAct model.drugs))
        ]
    <|
        navButtons model


mechOfAct drug =
    let
        mechanisms =
            (AedInfo.aedinfo drug).mechanism
    in
        [ column [ alignLeft, paddingEach { top = 0, bottom = 12, left = 0, right = 0 } ]
            [ el [ Font.heavy, Font.color (color SecondaryDark), paddingXY 0 2 ] (Element.text (Debug.toString drug))
            , column [] <|
                (List.map
                    (\( x, y ) ->
                        (el [ Font.regular, Font.size 18, paddingXY 0 4 ]
                            (Element.text
                                ((Debug.toString x)
                                    ++ "  "
                                    ++ (case y of
                                            AedInfo.OnePlus ->
                                                "+"

                                            AedInfo.TwoPlus ->
                                                "++"

                                            AedInfo.ThreePlus ->
                                                "+++"
                                       )
                                )
                            )
                        )
                    )
                    mechanisms
                )
            ]
        ]


warfntrxnView model =
    cardBasic
        [ (cardTitleBox "Warfarin Interactions")
        , column
            [ centerX
            , width (px 320)
            , paddingXY 0 10
            , Font.size 20
            ]
            (List.map (\drug -> (column [] [ column [ spacing 6 ] [ el [ Font.heavy, Font.color (color SecondaryDark) ] (Element.text (Debug.toString drug)), paragraph [ paddingEach { top = 0, bottom = 16, left = 0, right = 0 } ] [ el [ paddingEach { top = 0, bottom = 8, left = 0, right = 0 } ] (Element.text (AedInfo.warfToString (AedInfo.aedinfo drug).warfarin)) ] ] ])) model.drugs)
        ]
    <|
        navButtons model


ocpfxView model =
    cardBasic
        [ (cardTitleBox "OCP Interactions")
        , column
            [ centerX
            , width (px 320)
            , paddingXY 0 10
            , Font.size 20
            ]
            (List.map (\drug -> (column [] [ column [ spacing 6 ] [ el [ Font.heavy, Font.color (color SecondaryDark) ] (Element.text (Debug.toString drug)), paragraph [ paddingEach { top = 0, bottom = 16, left = 0, right = 0 } ] [ el [ paddingEach { top = 0, bottom = 8, left = 0, right = 0 } ] (Element.text (AedInfo.ocpfxToString (AedInfo.aedinfo drug).ocpfx)) ] ] ])) model.drugs)
        ]
    <|
        navButtons model


pregDataView model =
    cardBasic
        [ (cardTitleBox "Pregnancy Data")
        , column
            [ centerX
            , width (px 320)
            , paddingXY 0 10
            , Font.size 20
            ]
            (List.map (\drug -> (column [] [ column [ spacing 6 ] [ el [ Font.heavy, Font.color (color SecondaryDark) ] (Element.text (Debug.toString drug)), paragraph [ paddingEach { top = 0, bottom = 16, left = 0, right = 0 } ] [ el [ paddingEach { top = 0, bottom = 8, left = 0, right = 0 } ] (Element.text (AedInfo.pregToString (AedInfo.aedinfo drug).preg)) ] ] ])) model.drugs)
        , newTabLink [ centerX, Font.heavy, Font.color (color TertiaryDark) ]
            { url = "http://www.aedpregnancyregistry.org/wp-content/uploads/hernandez_diaz_neurology_2012.pdf"
            , label = Element.text "Source"
            }
        ]
    <|
        navButtons model
