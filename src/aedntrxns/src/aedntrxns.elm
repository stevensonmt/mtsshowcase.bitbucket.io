module AedNtrxns exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input
import Element.Lazy
import AedInfo exposing (..)
import List.Extra exposing (permutations, unique, uniqueBy)
import Models exposing (..)
import View exposing (..)
import Update exposing (..)
import Msgs exposing (..)
import Fonts exposing (..)


main =
    Browser.sandbox { init = init, update = update, view = view }


init : Model
init =
    { drugs = [], topic = Models.None }
