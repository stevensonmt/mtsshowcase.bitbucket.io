extern crate warp;
extern crate reqwest;
extern crate rand;
extern crate tera;
extern crate log;
extern crate pretty_env_logger;
#[macro_use]
extern crate serde_json;
extern crate hyper;
extern crate handlebars;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate alcoholic_jwt;
extern crate data_encoding;

use std::error::Error;
use std::sync::Arc;
use warp::Filter;
use handlebars::Handlebars;
use serde::Serialize;
use std::env;
use warp::*;
use reqwest::*;
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use alcoholic_jwt::*;


fn main() {
    if env::var_os("RUST_LOG").is_none() {
        // Set `RUST_LOG=todos=debug` to see debug logs,
        // this only shows access logs.
        env::set_var("RUST_LOG", "todos=info");
    }
    pretty_env_logger::init();


    let root = warp::path::end().map(|| "This is the root of the app.");

    let hello = path!("hello" / String).map(|name| format!("Hello, {}!", name));

    let hi = warp::path("hi")
        .and(warp::filters::path::full())
        .map(|pathstr: warp::filters::path::FullPath| {
                 let fragments = pathstr.as_str();
                 format!("Hi there, {:?}", fragments)
             });


    let mycv = warp::path("cv").and(warp::fs::file("./src/mycv/myCV.html"));

    let aedntrxn = warp::path("aed").and(warp::fs::file("./src/aedntrxns/index.html"));

    let listopf = warp::path("listopf");

    let listopfroot = listopf
        .and(warp::path::end())
        .and(warp::fs::file("./src/todo/index.html"));

    struct WithTemplate<T: Serialize> {
        name: &'static str,
        value: T,
    }

    fn render<T>(template: WithTemplate<T>, hbs: Arc<Handlebars>) -> impl warp::Reply
        where T: Serialize
    {
        hbs.render(template.name, &template.value)
            .unwrap_or_else(|err| err.description().to_owned())
    }



    let template = r#"<!DOCTYPE HTML>
                    <html>
                    <head>
                      <meta charset="UTF-8">
                      <title>Listo Para Faena</title>
                    </head>

                    <body>
                      <p><a href="https://listopf.auth0.com/authorize?response_type=code&client_id={{client_id}}&redirect_uri={{redirect_uri}}&scope=openid%20profile&state={{state}}">SIGNUP</a></p>
                    </body>
                    </html>"#;

    let mut hb = Handlebars::new();
    // register the template
    hb.register_template_string("template.html", template)
        .unwrap();

    // Turn Handlebars instance into a Filter so we can combine it
    // easily with others...
    let hb = Arc::new(hb);

    // Create a reusable closure to render template
    let handlebars = move |with_template| render(with_template, hb.clone());

    let listopflogin = listopf
        .and(warp::path("login"))
        .map(|| {
            WithTemplate {
                name: "template.html",
                value:
                    json!({"client_id" : AUTH0, "state" : state(), "redirect_uri" : REDIRECT_URI}),
            }
        })
        .map(handlebars);

    let listopfassets = warp::fs::dir("todo");

    let listopfcallback =
        listopf
            .and(warp::path("callback"))
            .and(warp::filters::query::raw())
            .map(|code: String| AccessCode::from_query_str(&code))
            .map(|accesscode: AccessCode| {
                println!("{}", accesscode.to_string());
                OauthToken::from_access_code(accesscode)
            })
            .map(|resp: Result<reqwest::Response>| {
                let mut unwrapped = resp.unwrap();
                println!("{:?}", unwrapped);
                unwrapped.json::<OauthToken>()
            })
            .map(|token: Result<OauthToken>| {
                println!("token: {:?}", token);
                let id_token = &token.unwrap().id_token;
                println!("{:?}", alcoholic_jwt::token_kid(id_token));
                id_token.to_string()
            })
            .map(|token: String| {
                alcoholic_jwt::validate(&token,
                                        &jwk_by_kid(auth0_jwks(),
                                                    &alcoholic_jwt::token_kid(&token)
                                                         .unwrap()
                                                         .unwrap()),
                                        vec![])
                        .unwrap()
            })
            .map(|valid_jwt: ValidJWT| format!("jwt claims: {:?}", valid_jwt.claims));
    //.map(|encoded: biscuit::JWT<biscuit::ClaimsSet<MyClaimsSet>, biscuit::Empty>| {println!("{:?}", encoded.clone().unwrap_encoded().parts[1]); encoded})//; encoded.decode(&(Secret::Bytes("nIALb48CsUhuYYNOMu1NHhtVidZ8nr0R6T1iAyWmQEwYIbTuCo7XJxXN5GfA36Sh".to_string().into_bytes())), SignatureAlgorithm::RS256)})
    //.map(|encoded: biscuit::JWT<biscuit::ClaimsSet<MyClaimsSet>, biscuit::Empty>| format!("what???  {:?}", (encoded.unwrap_encoded().parts[2])));
    //format!("{:?}", code.split('&').collect::<Vec<&str>>().iter().map(|frag| frag.split('=').collect::<Vec<&str>>()).collect::<Vec<Vec<&str>>>()));

    let routes = root.or(mycv)
        .or(hi)
        .or(hello)
        .or(aedntrxn)
        .or(listopfroot)
        .or(listopfassets)
        .or(listopflogin)
        .or(listopfcallback)
        .with(warp::log("todos"));

    println!("jwk by kid: {:?}", jwk_by_kid(auth0_jwks(), "MDRFQTkxNEM3RDAxNzczOTdGM0M4MEVEQjFBQjYyMDE0MzhFMTdFNA"));
    println!("validate: {:?}", alcoholic_jwt::validate("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1EUkZRVGt4TkVNM1JEQXhOemN6T1RkR00wTTRNRVZFUWpGQlFqWXlNREUwTXpoRk1UZEZOQSJ9.eyJnaXZlbl9uYW1lIjoiTWF0dGhldyIsImZhbWlseV9uYW1lIjoiU3RldmVuc29uIiwibmlja25hbWUiOiJzdGV2ZW5zb25tdCIsIm5hbWUiOiJNYXR0aGV3IFN0ZXZlbnNvbiIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLV9pSFM4U01HcUhBL0FBQUFBQUFBQUFJL0FBQUFBQUFBQWJFL0h1ZGVicVExSUY0L3Bob3RvLmpwZyIsImdlbmRlciI6Im1hbGUiLCJsb2NhbGUiOiJlbiIsInVwZGF0ZWRfYXQiOiIyMDE5LTAxLTIzVDE0OjI0OjM5LjExN1oiLCJpc3MiOiJodHRwczovL2xpc3RvcGYuYXV0aDAuY29tLyIsInN1YiI6Imdvb2dsZS1vYXV0aDJ8MTExNDAxOTgyODA0MzEzNDIxODkyIiwiYXVkIjoibzZvakw2ZllVTEFpTzhhMjRzc2xhSVprRTVuZHFsSHoiLCJpYXQiOjE1NDgzOTM3MjQsImV4cCI6MTU0ODQyOTcyNH0.PScbLeS1FXGA_vvwMjtP8HstBicOOjDFpnbCwu0pMdNY23MM0OI60WXpgauZhRyjUDAbGRekH5iEu-4KfQcpnXRyF_ha40uejbTensdJU_b_kBhurPxD6zyu---XrLGoasJvLvNbc9Pkt-HYjavJxHx-C-u09UUVPfp5B5NbvaV775AP2gDVEYARSOp21GjD78bRvkgdgzxNaVqHNT51yv-UnRsNb5fPrxS4pSCA9RslPkXwDK_aruwuHPaDjXciKTjOPnJGVF2ccgoAWcJvF5R1OZxvAunTyIiHbUz7zydkhWE5ilPiN4OxRuBR_CfGSH5L8x9rzoL4FzMe6Xx_Jg", &jwk_by_kid(auth0_jwks(), "MDRFQTkxNEM3RDAxNzczOTdGM0M4MEVEQjFBQjYyMDE0MzhFMTdFNA"), vec![]).unwrap().claims);

    warp::serve(routes).run(([127, 0, 0, 1], 3030));
}

const GOOGLEOAUTH: &'static str = "805435651182-uhfouihs12fl8i1cb52ttvubeaupjurp.apps.googleusercontent.com";

const AUTH0: &'static str = "o6ojL6fYULAiO8a24sslaIZkE5ndqlHz";

const REDIRECT_URI: &'static str = "http://localhost:3030/listopf/callback";

fn state() -> String {
    thread_rng().sample_iter(&Alphanumeric).take(12).collect()
}

#[derive(Serialize,Deserialize,Debug)]
struct AccessCode {
    code: String,
    state: String,
}

impl AccessCode {
    fn new() -> Self {
        AccessCode {
            code: "".to_string(),
            state: "".to_string(),
        }
    }

    fn to_string(&self) -> String {
        format!("{{ \"code\": {},
           \"state\": {}
         }}", self.code, self.state)
    }

    fn from_query_str(query: &str) -> AccessCode {
        let mut accesscode = AccessCode::new();
        query
            .split('&')
            .map(|frag| frag.split('=').collect::<Vec<&str>>())
            .collect::<Vec<Vec<&str>>>()
            .iter()
            .map(|pair| match pair[0] {
                     "code" => accesscode.code = pair[1].to_string(),
                     "state" => accesscode.state = pair[1].to_string(),
                     _ => (),
                 })
            .for_each(drop);
        accesscode
    }
}

#[derive(Debug,Serialize,Deserialize,Clone)]
struct OauthToken {
    access_token: String,
    id_token: String,
    token_type: String,
    scope: Option<String>,
    expires_in: Option<u32>,
}

impl OauthToken {
    fn new() -> Self {
        OauthToken {
            access_token: "".to_string(),
            id_token: "".to_string(),
            token_type: "".to_string(),
            scope: Some("".to_string()),
            expires_in: Some(0),
        }
    }

    fn from_access_code(accesscode: AccessCode) -> Result<reqwest::Response> {
        println!("{:?}", accesscode.code);
        let client = reqwest::Client::new();
        client.post("https://listopf.auth0.com/oauth/token")
                        .header(reqwest::header::CONTENT_TYPE, "application/json")
                        .body(format!("{{\"grant_type\":\"authorization_code\",\"client_id\": \"{}\",\"client_secret\": \"nIALb48CsUhuYYNOMu1NHhtVidZ8nr0R6T1iAyWmQEwYIbTuCo7XJxXN5GfA36Sh\",\"code\": \"{}\",\"redirect_uri\": \"{}\"}}", AUTH0, accesscode.code, REDIRECT_URI ))
                        .send()

    }
}

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
struct MyClaimsSet {
    given_name: String,
    family_name: String,
    nickname: String,
    picture: String,
    gender: String,
    locale: String,
    updated_at: String,
    iss: String,
    sub: String,
    aud: String,
    iat: u32,
    exp: u32,
}


fn auth0_jwks() -> alcoholic_jwt::JWKS {
    let resp = reqwest::get("https://listopf.auth0.com/.well-known/jwks.json")
        .unwrap()
        .text()
        .unwrap_or("".to_string());


    serde_json::from_str(&resp).unwrap()
}

fn jwk_by_kid(jwks: alcoholic_jwt::JWKS, kid: &str) -> alcoholic_jwt::JWK {
    jwks.find(kid).unwrap().clone()
}
