pub mod login {
    extern crate warp;
    extern crate hyper;
    extern crate handlebars;

    extern crate serde;

    use std::error::Error;
    use std::sync::Arc;

    use warp::Filter;
    use self::handlebars::Handlebars;
    use self::serde::Serialize;

    struct WithTemplate<T: Serialize> {
        name: &'static str,
        value: T,
    }

    fn render<T>(template: WithTemplate<T>, hbs: Arc<Handlebars>) -> impl warp::Reply
        where T: Serialize
    {
        hbs.render(template.name, &template.value)
            .unwrap_or_else(|err| err.description().to_owned())
    }

    fn template(path: &'static str) {
        let template = r#"<!DOCTYPE HTML>
                    <html>
                    <head>
                      <meta charset="UTF-8">
                      <title>Listo Para Faena</title>
                      <link rel="stylesheet" href="whatever-you-want.css">
                      <script src="elm.js"></script>
                      <script src="https://cdn.auth0.com/js/auth0/9.5.1/auth0.min.js"></script>
                    </head>

                    <body>
                      <div id="elm"></div>
                      <p><a href="https://listopf.auth0.com/authorize?response_type=id_token token&client_id=o6ojL6fYULAiO8a24sslaIZkE5ndqlHz&redirect_uri=http://localhost:3030/listopf&scope=openid%20profile&state=xyzABC123&nonce=NONCE">SIGNUP</a></p>
                      <script>
                      var app = Elm.Main.init({
                        node: document.getElementById('elm'),
                        flags: {
                            width: window.innerWidth,
                            height: window.innerHeight
                        }
                      });
                     
                      </script>
                      <script type="text/javascript">
                      var webAuth = new auth0.WebAuth({
                        domain:       'listopf.auth0.com',
                        clientID:     'o6ojL6fYULAiO8a24sslaIZkE5ndqlHz'
                      });
                      webAuth.authoriz({ connection: 'google-oauth2'})
                    </script>
                    </body>
                    </html>"#;

        let mut hb = Handlebars::new();
        // register the template
        hb.register_template_string("template.html", template)
            .unwrap();

        // Turn Handlebars instance into a Filter so we can combine it
        // easily with others...
        let hb = Arc::new(hb);

        // Create a reusable closure to render template
        let handlebars = move |with_template| render(with_template, hb.clone());
        let route = warp::path(path)
            .map(|| {
                     WithTemplate {
                         name: "template.html",
                         value: json!({"user" : "USER"}),
                     }
                 })
            .map(handlebars);
    }
}
