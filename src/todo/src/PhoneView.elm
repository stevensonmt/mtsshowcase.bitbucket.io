module PhoneView exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Input as Input
import Element.Font as Font
import Html.Attributes
import Html.Events exposing (stopPropagationOn)
import Json.Decode as Json
import Dict
import Browser.Dom
import Task
import Colors
import Msgs
import Models exposing (ViewModel)
import Chore exposing (ChoreDict)
import Buttons exposing (..)
import DesktopView exposing (selectedView)


baseView model choresDict currChoreID =
    (case model.device.orientation of
        Portrait ->
            column
                [ width <| fill
                , height <| fill
                , centerX
                , alignTop
                , padding 20
                , spacing 10
                , Background.color (Colors.secondary 1)
                , Border.rounded 28
                , Border.shadow
                    { offset = ( 1, 1 )
                    , size = 1
                    , blur = 6
                    , color = Colors.primary 0.3
                    }
                ]
                [ row
                    [ width <| fill
                    , height <| fillPortion 9
                    ]
                    [ selectedView model choresDict currChoreID ]
                , row
                    [ width <| fill
                    , height <| fillPortion 1
                    ]
                    [ mainMenu model choresDict ]
                ]

        _ ->
            row
                [ width <| fill
                , height <| fill

                --, padding 14
                --, spacing 8
                , Background.color (Colors.primary 1)
                , Border.rounded 28
                , Border.shadow
                    { offset = ( 1, 1 )
                    , size = 1
                    , blur = 6
                    , color = Colors.primary 0.3
                    }
                ]
                [ el
                    [ height fill
                    , width <| fillPortion 1
                    , Background.color <| Colors.secondary 1
                    , Border.roundEach { topLeft = 28, bottomLeft = 28, topRight = 0, bottomRight = 0 }
                    , centerY
                    ]
                  <|
                    landscapeMainMenu model choresDict
                , el
                    [ height fill
                    , width <| fillPortion 7
                    , Background.color (Colors.primary 1)
                    , Border.roundEach { topLeft = 0, bottomLeft = 0, topRight = 28, bottomRight = 28 }
                    , clip
                    ]
                  <|
                    selectedView model choresDict currChoreID
                ]
    )


selectedView : ViewModel -> ChoreDict -> Maybe Int -> Element Msgs.MainMsg
selectedView model choresDict currChoreID =
    case model.device.orientation of
        Portrait ->
            DesktopView.selectedView model choresDict currChoreID

        _ ->
            case model.currentView of
                Models.ListItems itemstate ->
                    let
                        ( title, choreList ) =
                            case itemstate of
                                Msgs.Any ->
                                    ( "All Chores", (Dict.toList choresDict) )

                                Msgs.Active ->
                                    ( "Active Chores", (List.filter (\( id, chore ) -> not chore.completed || chore.deleted) (Dict.toList choresDict)) )

                                Msgs.Complete ->
                                    ( "Finished Chores", (List.filter (\( id, chore ) -> chore.completed) (Dict.toList choresDict)) )

                                Msgs.Deleted ->
                                    ( "Deleted Chores", (List.filter (\( id, chore ) -> chore.deleted) (Dict.toList choresDict)) )

                        viewport =
                            model.dimensions
                    in
                        column
                            [ width fill
                            , paddingXY 10 4
                            , spacing 8
                            , height fill
                            ]
                            ([ row
                                [ centerX
                                ]
                                [ Element.text title ]
                             ]
                                ++ ([ wrappedRow [ height <| fill, spacing 10 ]
                                        (List.map
                                            (\( id, task ) ->
                                                Element.el
                                                    ([ Element.htmlAttribute <| Html.Attributes.attribute "tabindex" "-1"
                                                     , Events.onLoseFocus <| Msgs.UpdView (Msgs.Menuize Msgs.None)
                                                     , Events.onClick <|
                                                        (Msgs.UpdView (Msgs.Menuize (Msgs.ItemMenu id)))
                                                     , pointer
                                                     , width <| px <| ((viewport.width) // 5)
                                                     , paddingXY 8 4
                                                     ]
                                                        ++ (if model.openMenu == Msgs.ItemMenu id then
                                                                [ Element.inFront (itemMenu id choresDict model.cacheName model.cacheDesc)
                                                                ]
                                                                    ++ activeItemStyle
                                                            else if task.deleted then
                                                                deletedItemStyle
                                                            else if task.completed then
                                                                completedItemStyle
                                                            else
                                                                []
                                                           )
                                                    )
                                                    (let
                                                        leader =
                                                            if itemstate == Msgs.Any then
                                                                String.fromChar '♦'
                                                            else
                                                                String.fromChar
                                                                    (if task.completed then
                                                                        '✓'
                                                                     else if task.deleted then
                                                                        '✘'
                                                                     else
                                                                        '♦'
                                                                    )
                                                     in
                                                        Element.text <| leader ++ " " ++ task.name
                                                    )
                                            )
                                            choreList
                                        )
                                    ]
                                   )
                            )

                Models.Editing ->
                    column
                        [ height fill
                        , width fill
                        , paddingXY 10 0
                        , centerX
                        , Font.color (Colors.tertiary 1)
                        , spacing 10
                        ]
                        [ el [] (Element.text ("Editing current item id#: " ++ (Debug.toString currChoreID)))
                        , (Input.text [ Font.color <| Colors.primary 1, Border.rounded 8, Background.color (Colors.tertiary 1), Element.focused [ Background.color (Element.rgb255 245 242 238), Border.shadow { offset = ( 0, 0 ), size = 1, blur = 8, color = (Colors.primary 0.5) } ], Input.focusedOnLoad, Element.htmlAttribute (Html.Attributes.id "task name input") ]
                            { onChange = \text -> Msgs.UpdView (Msgs.CacheName text)
                            , text = model.cacheName
                            , placeholder = Nothing
                            , label = Input.labelAbove [ Font.size 14 ] (Element.text "Add Task Name Here")
                            }
                          )
                        , (Input.text [ Font.color <| Colors.primary 1, Border.rounded 8, Background.color (Colors.tertiary 1), Element.focused [ Background.color (Element.rgb255 245 242 238), Border.shadow { offset = ( 0, 0 ), size = 1, blur = 8, color = (Colors.primary 0.5) } ] ]
                            { onChange = \text -> Msgs.UpdView (Msgs.CacheDesc text)
                            , text = model.cacheDesc
                            , placeholder = Nothing
                            , label = Input.labelAbove [ Font.size 14 ] (Element.text "Add Task Description Here")
                            }
                          )
                        , Input.button
                            [ Border.rounded 8
                            , Background.color (Colors.primary 1.0)
                            , Font.color
                                (if String.isEmpty model.cacheName then
                                    Colors.secondary 0.3
                                 else
                                    Element.rgb255 245 242 238
                                )
                            , Font.bold
                            , Font.size 24
                            , Font.variant Font.smallCaps
                            , padding 10
                            , Element.focused [ Background.color (Colors.primary 0.8) ]
                            ]
                            { onPress =
                                (if String.isEmpty model.cacheName then
                                    Nothing
                                 else
                                    case currChoreID of
                                        Just x ->
                                            Just (Msgs.UpdData (Msgs.EditChore x model.cacheName model.cacheDesc))

                                        _ ->
                                            Nothing
                                )
                            , label = Element.text "Submit"
                            }
                        ]

                Models.AddNew ->
                    el [] Element.none

                _ ->
                    el [] (Element.none)


activeItemStyle =
    [ Font.bold ]


deletedItemStyle =
    [ Font.color (Element.rgba255 202 81 76 0.8) ]


completedItemStyle =
    [ Font.color (Element.rgba255 76 202 103 0.8) ]


mainMenu model choresDict =
    row [ centerX ]
        [ el
            [ Border.rounded 16
            , width (px 16)
            , height (px 16)
            , alignBottom
            , Element.moveUp 32
            , centerX
            , Element.scale 3
            , Element.htmlAttribute (Html.Attributes.id "mainMenu")
            , Element.htmlAttribute <| Html.Attributes.attribute "tabindex" "-1"
            , Events.onLoseFocus <| Msgs.UpdView (Msgs.Menuize Msgs.None)
            , pointer
            , Events.onClick <| Msgs.UpdView (Msgs.Menuize Msgs.MainMenu)
            , Element.above
                (if model.openMenu == Msgs.MainMenu then
                    (row
                        [ width (px 100)
                        , height (px 40)
                        , Element.moveDown 12
                        , centerX --Element.moveLeft 40
                        , Element.behindContent
                            (Element.image [ Element.scale 1 ]
                                { src = "src/Icons/arcmenu.svg"
                                , description = "background for main menu"
                                }
                            )
                        , Events.onMouseLeave (Msgs.UpdView (Msgs.Menuize Msgs.None))
                        ]
                        (List.map
                            (\btn ->
                                el
                                    --Input.button
                                    ([ width fill
                                     , Border.rounded 16
                                     , Element.htmlAttribute <| Html.Events.stopPropagationOn "click" (Json.succeed <| ( btn.action, True ))
                                     ]
                                        ++ mainMenuButtonPosition btn
                                    )
                                    (Element.image
                                        [ width <| px 16
                                        , height <| px 16
                                        , Border.rounded 16
                                        , Element.scale 1
                                        ]
                                        { src = "src/Icons/" ++ btn.src
                                        , description = btn.description
                                        }
                                    )
                            )
                            (Buttons.buttons (Buttons.buttonList model.currentView) choresDict)
                        )
                    )
                 else
                    Element.none
                )
            ]
            (Element.image
                [ pointer ]
                { src = "src/Icons/burgermenuicon.svg"
                , description = "open main menu icon"
                }
            )
        ]


mainMenuButtonPosition : Button -> List (Element.Attribute msg)
mainMenuButtonPosition button =
    case button.action of
        Msgs.UpdData Msgs.AddChore ->
            [ alignLeft
            , alignBottom
            , moveUp 4
            , moveRight 4
            ]

        Msgs.UpdView (Msgs.Itemize Msgs.Any) ->
            [ moveLeft ((+) 5 <| (*) 8 <| cos <| degrees 150)
            , moveUp ((*) 7 <| sin <| degrees 150)
            ]

        Msgs.UpdData Msgs.SweepAll ->
            [ moveLeft ((+) 5 <| (*) 8 <| cos <| degrees 150)
            , moveUp ((*) 7 <| sin <| degrees 150)
            ]

        Msgs.UpdView (Msgs.Itemize Msgs.Deleted) ->
            [ moveLeft ((+) -10.2 <| (*) 8 <| cos <| degrees 30)
            , moveUp ((+) -1 <| (*) 8 <| sin <| degrees 30)
            ]

        Msgs.UpdData Msgs.SweepDel ->
            [ moveLeft ((+) -10.2 <| (*) 8 <| cos <| degrees 30)
            , moveUp ((+) -1 <| (*) 8 <| sin <| degrees 30)
            ]

        Msgs.UpdView (Msgs.Itemize Msgs.Complete) ->
            [ alignLeft
            , moveLeft ((+) -3 <| (*) 8 <| cos <| degrees 90)
            , moveUp ((*) 8 <| sin <| degrees 90)
            ]

        Msgs.UpdData Msgs.SweepComp ->
            [ alignLeft
            , moveLeft ((+) -3 <| (*) 8 <| cos <| degrees 90)
            , moveUp ((*) 8 <| sin <| degrees 90)
            ]

        Msgs.Rollback ->
            [ alignRight
            , alignBottom
            , moveUp 4
            , moveLeft -0.8
            ]

        _ ->
            []


landscapeMainMenu model choresDict =
    column
        [ centerX
        , paddingXY 0 18
        , spaceEvenly
        , height fill
        , width fill
        ]
        (List.map
            (\btn ->
                --el
                --Input.button
                {--([ width fill--}
                --, Border.rounded 16
                --, Element.htmlAttribute <| Html.Events.stopPropagationOn "click" (Json.succeed <| ( btn.action, True ))
                --]
                --++ mainMenuButtonPosition btn
                {--)--}
                (Element.image
                    [ width <| px 16
                    , height <| px 16
                    , Border.rounded 16
                    , centerX
                    , Element.scale 2
                    , Element.htmlAttribute <| Html.Events.stopPropagationOn "click" (Json.succeed <| ( btn.action, True ))
                    ]
                    { src = "src/Icons/" ++ btn.src
                    , description = btn.description
                    }
                )
            )
            (Buttons.buttons (Buttons.buttonList model.currentView) choresDict)
        )


itemMenu currentChoreID choreDict cacheName cacheDesc =
    row
        [ Background.color <| Colors.tertiary 0.7
        , Border.rounded 20
        , Element.moveUp 20
        , centerX
        , centerY
        , width fill
        , spaceEvenly
        ]
        [ Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.secondary 0.0)
            , Element.focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , Element.scale 0.8
            , pointer
            , Events.onClick <| Msgs.UpdView Msgs.Editing
            ]
            { src = "src/Icons/editicon.svg", description = "icon to edit item" }
        , Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.primary 0.0)
            , focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , pointer
            , Element.scale 0.8
            , Element.htmlAttribute <| stopPropagationOn "click" (Json.succeed ( (Msgs.UpdData (Msgs.CompleteChore currentChoreID)), True ))
            ]
            { src = "src/Icons/checkmark.svg", description = "icon to mark item complete" }
        , Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.primary 0.0)
            , focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , pointer
            , Element.scale 0.8
            , Element.htmlAttribute <| stopPropagationOn "click" (Json.succeed ( (Msgs.UpdData (Msgs.DeleteChore currentChoreID)), True ))
            ]
            { src = "src/Icons/trashicon.svg", description = "icon to delete item" }
        ]
