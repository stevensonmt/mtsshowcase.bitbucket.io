module Main exposing (..)

import Browser
import Browser.Dom as Dom
import Browser.Events
import Html
import Html.Attributes exposing (id)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Element.Events
import Models exposing (..)
import Chore exposing (..)
import Task
import Colors exposing (..)
import Fonts exposing (..)
import Dict exposing (..)
import Time exposing (..)
import Views exposing (..)
import Msgs exposing (..)


main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }


subscriptions model =
    Browser.Events.onResize <|
        \width height ->
            UpdView (Msgs.ClassifyDevice { width = width, height = height })



--(Element.classifyDevice { width = width, height = height }))
-- MODEL --


type alias Model =
    Models.Model



-- INIT --


init : Msgs.Flags -> ( Model, Cmd msg )
init flags =
    ( { tasks = Dict.empty
      , currentChoreID = Nothing
      , lastEdit = NoEdit
      , openMenu = None
      , currentView = Views.initialView flags
      }
    , Cmd.none
    )



-- VIEW --


view : Model -> Html.Html MainMsg
view model =
    Views.view
        model.currentView
        model.tasks
        model.currentChoreID


update : MainMsg -> Model -> ( Model, Cmd MainMsg )
update msg model =
    case msg of
        GetTime time ->
            let
                id =
                    Time.posixToMillis time

                mdl =
                    { model | currentChoreID = Just id }
            in
                (update (UpdView Msgs.Editing) mdl)

        UpdData choremsg ->
            case choremsg of
                AddChore ->
                    ( model, Task.perform GetTime Time.now )

                EditChore id name desc ->
                    ( { model | lastEdit = Previous model, tasks = Chore.update (EditChore id name desc) model.tasks, currentView = Views.update (Itemize Msgs.Any) model.currentView, currentChoreID = Nothing }, Cmd.none )

                Msgs.SweepAll ->
                    let
                        currview =
                            model.currentView
                    in
                        ( { model
                            | tasks = (Dict.empty)
                            , currentView = { currview | currentView = ListItems Msgs.Any, openMenu = None }
                            , lastEdit = Previous model
                          }
                        , Cmd.none
                        )

                Msgs.SweepComp ->
                    let
                        incomplete =
                            Dict.filter (\id task -> (not task.completed)) model.tasks

                        currview =
                            model.currentView
                    in
                        ( { model
                            | tasks = incomplete
                            , currentView = { currview | openMenu = None }
                            , lastEdit = Previous model
                          }
                        , Cmd.none
                        )

                Msgs.SweepDel ->
                    let
                        nonDeleted =
                            Dict.filter (\id task -> (not task.deleted)) model.tasks

                        currview =
                            model.currentView
                    in
                        ( { model
                            | tasks = nonDeleted
                            , currentView = { currview | openMenu = None }
                            , lastEdit = Previous model
                          }
                        , Cmd.none
                        )

                _ ->
                    ( { model | lastEdit = Previous model, tasks = Chore.update choremsg model.tasks, currentView = Views.update (Menuize None) model.currentView }, Cmd.none )

        Msgs.UpdView viewmsg ->
            let
                cmd =
                    case viewmsg of
                        Msgs.Editing ->
                            (Task.attempt (\_ -> NoOp) (Dom.focus model.currentView.focusedElement))

                        _ ->
                            Cmd.none

                currID =
                    case viewmsg of
                        Menuize (ItemMenu id) ->
                            Just id

                        _ ->
                            model.currentChoreID

                chore =
                    case Dict.get (Maybe.withDefault 0 currID) model.tasks of
                        Just chr ->
                            chr

                        _ ->
                            Chore.new

                currView =
                    model.currentView

                updatedView =
                    case viewmsg of
                        Msgs.Editing ->
                            { currView
                                | cacheName = chore.name
                                , cacheDesc = chore.description
                                , currentView = Models.Editing
                                , focusedElement = "task name input"
                                , openMenu = Msgs.None
                            }

                        _ ->
                            Views.update viewmsg currView
            in
                ( { model
                    | currentView = updatedView
                    , currentChoreID = currID
                  }
                , cmd
                )

        Rollback ->
            let
                mdl =
                    case model.lastEdit of
                        Previous mod ->
                            mod

                        NoEdit ->
                            model
            in
                ( { mdl
                    | currentChoreID = Nothing
                    , currentView =
                        (if (mdl.lastEdit == NoEdit || model.currentView.currentView == Models.Editing) then
                            Views.update (Itemize Any) model.currentView
                         else
                            model.currentView
                        )
                    , openMenu = None
                  }
                , Cmd.none
                )

        NoOp ->
            ( model, Cmd.none )
