module Auth exposing (..)

import Auth0 exposing (Endpoint, Auth0Config)


myDomain : Endpoint
myDomain =
    "listopf.auth0.com"


myClientID : String
myClientID =
    "o6ojL6fYULAiO8a24sslaIZkE5ndqlHz"


myAuth0Config : Auth0Config
myAuth0Config =
    Auth0Config myDomain myClientID
