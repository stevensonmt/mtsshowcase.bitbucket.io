module Buttons exposing (..)

import Msgs
import Models
import Chore exposing (ChoreDict)
import Dict


type alias Button =
    { action : Msgs.MainMsg
    , src : String
    , title : String
    , description : String
    }


type Btn
    = AddNewChore
    | ListAny
    | ListDel
    | ListComp
    | SweepAll
    | SweepDel
    | SweepComp
    | Undo


buttons : List Btn -> ChoreDict -> List Button
buttons btnlist choresDict =
    {--let--}
    --dictState =
    --{ empty = Dict.isEmpty choresDict
    --, anyComplete = Dict.isEmpty (Dict.filter (\k v -> v.completed) choresDict)
    --, anyDeleted = Dict.isEmpty (Dict.filter (\k v -> v.deleted) choresDict)
    --}
    {--in--}
    List.map
        (\btn ->
            let
                ( action, ( src, title ), description ) =
                    case btn of
                        AddNewChore ->
                            ( Msgs.UpdData Msgs.AddChore
                            , ( "addicon.svg"
                              , "Add New Chore"
                              )
                            , "button to create new chore"
                            )

                        ListAny ->
                            ( (Msgs.UpdView (Msgs.Itemize Msgs.Any))
                            , ( "viewallicon.svg"
                              , "View All Chores"
                              )
                            , "button to view all chores"
                            )

                        ListDel ->
                            {--( if dictState.anyDeleted then--}
                            --Msgs.NoOp
                            {--else--}
                            ( Msgs.UpdView (Msgs.Itemize Msgs.Deleted)
                              --)
                            , ( "viewdeleted.svg"
                              , "View Deleted Chores"
                              )
                            , "button to view deleted chores"
                            )

                        ListComp ->
                            ( (Msgs.UpdView (Msgs.Itemize Msgs.Complete))
                            , ( "viewcompleted.svg"
                              , "View Completed Chores"
                              )
                            , "button to view completed chores"
                            )

                        SweepAll ->
                            ( (Msgs.UpdData (Msgs.SweepAll))
                            , ( "clearallicon.svg"
                              , "Remove All Chores"
                              )
                            , "button to remove all chores"
                            )

                        SweepDel ->
                            ( (Msgs.UpdData (Msgs.SweepDel))
                            , ( "cleardeletedicon.svg"
                              , "Remove Deleted Chores"
                              )
                            , "button to remove deleted chores"
                            )

                        SweepComp ->
                            ( (Msgs.UpdData (Msgs.SweepComp))
                            , ( "clearcompleteicon.svg"
                              , "Remove Complete Chores"
                              )
                            , "button to remove completed chores"
                            )

                        Undo ->
                            ( Msgs.Rollback
                            , ( "undoicon.svg"
                              , "Undo Previous Change"
                              )
                            , "button to undo previous change"
                            )

                -- { TODO: add Btns for item menu actions}
            in
                { action = action, src = src, title = title, description = description }
        )
    <|
        btnlist


buttonList : Models.View -> List Btn
buttonList currView =
    case currView of
        Models.ListItems Msgs.Any ->
            [ AddNewChore, SweepAll, ListComp, ListDel, Undo ]

        Models.ListItems Msgs.Complete ->
            [ AddNewChore, ListAny, SweepComp, ListDel, Undo ]

        Models.ListItems Msgs.Deleted ->
            [ AddNewChore, ListAny, ListComp, SweepDel, Undo ]

        _ ->
            [ AddNewChore, ListAny, ListComp, ListDel, Undo ]
