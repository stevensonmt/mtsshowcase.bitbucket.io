module DesktopView exposing (..)

import Html
import Html.Attributes
import Html.Events exposing (stopPropagationOn)
import Json.Decode as Json
import Element exposing (..)
import Element.Events as Events
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Dict exposing (toList)
import Colors
import Msgs
import Fonts
import Models exposing (ViewModel, View)
import Buttons exposing (..)
import Chore exposing (ChoreDict)


baseView : ViewModel -> ChoreDict -> Maybe Int -> Element Msgs.MainMsg
baseView model choresDict currChoreID =
    row
        [ width <| fill
        , height <| fill
        , padding 20
        , spacing 10
        ]
        [ column
            [ width <| fillPortion 1
            , height <| fill
            , padding 10
            , spacing 6
            , Background.color (Colors.secondary 1)
            , Border.shadow
                { offset = ( 1, 1 )
                , size = 1.0
                , blur = 4
                , color = Colors.primary 0.5
                }
            , Font.color (Colors.primary 1)
            , Font.family [ Fonts.fontStack Fonts.PrimarySans ]
            ]
            [ el
                [ centerX
                , paddingEach { top = 20, bottom = 0, left = 0, right = 0 }
                , Font.size 32
                , Font.family [ Fonts.fontStack Fonts.Headline ]
                , Font.bold
                ]
                (Element.text "Listo p f")
            , mainMenu model.currentView (Buttons.buttons (Buttons.buttonList model.currentView) choresDict)
            ]
        , column
            [ width <| (fillPortion 3 |> minimum 400)
            , height <| fill
            , padding 20
            , spacing 10
            , Background.color (Colors.primary 1)
            , Border.shadow
                { offset = ( 1, 1 )
                , size = 1.0
                , blur = 4
                , color = Colors.primary 0.5
                }
            ]
            [ el [] (Element.text (Debug.toString model.device.class))
            , selectedView model choresDict currChoreID
            ]
        ]


mainMenu currentView buttonList =
    column
        [ spacing 16
        , centerY
        , height fill
        , padding 10
        , scrollbarY
        ]
        (List.map
            (\btn ->
                Input.button
                    [ width fill
                    , Border.rounded 28
                    ]
                    { onPress = Just btn.action
                    , label =
                        Element.row
                            [ width fill
                            , height <| px 28
                            , spacing 12
                            , centerY
                            ]
                            [ Element.image
                                [ width <| px 16
                                , height <| px 16
                                , Border.rounded 16
                                , Element.scale 2
                                , Element.moveRight 8
                                ]
                                { src = "src/Icons/" ++ btn.src
                                , description = btn.description
                                }
                            , paragraph
                                [ padding 8
                                , height <| px 36
                                , width <| px 258
                                , centerY
                                , spacing 8
                                ]
                                [ Element.text btn.title ]
                            ]
                    }
            )
            (buttonList)
        )


selectedView : ViewModel -> ChoreDict -> Maybe Int -> Element Msgs.MainMsg
selectedView viewmodel choresDict currentChoreID =
    case viewmodel.currentView of
        Models.ListItems itemstate ->
            let
                ( title, choreList ) =
                    case itemstate of
                        Msgs.Any ->
                            ( "All Chores", (Dict.toList choresDict) )

                        Msgs.Active ->
                            ( "Active Chores", (List.filter (\( id, chore ) -> not chore.completed || chore.deleted) (Dict.toList choresDict)) )

                        Msgs.Complete ->
                            ( "Finished Chores", (List.filter (\( id, chore ) -> chore.completed) (Dict.toList choresDict)) )

                        Msgs.Deleted ->
                            ( "Deleted Chores", (List.filter (\( id, chore ) -> chore.deleted) (Dict.toList choresDict)) )
            in
                column
                    [ width fill
                    , padding 12
                    , spacing 8
                    , Background.color (Colors.secondary 1)
                    , height fill

                    {--, Element.behindContent <|--}
                    --Element.image
                    --[ Element.scale 0.4
                    --, Element.alpha 0.8
                    --, Element.moveUp 160
                    --, Element.moveRight 240
                    ----, Events.onClick <|
                    ----Msgs.UpdView (Msgs.Menuize Msgs.None)
                    --]
                    --{ src = "src/Icons/monkeySeeLogo.svg"
                    --, description = ""
                    {--}
                    --}
                    ]
                    ([ row [ centerX, paddingXY 12 8 ] [ Element.text title ] ]
                        ++ (List.map
                                (\( id, task ) ->
                                    Element.el
                                        ([ Element.htmlAttribute <| Html.Attributes.attribute "tabindex" "-1"
                                         , Events.onLoseFocus <| Msgs.UpdView (Msgs.Menuize Msgs.None)
                                         , Events.onClick <|
                                            (Msgs.UpdView (Msgs.Menuize (Msgs.ItemMenu id)))
                                         , pointer
                                         ]
                                            ++ (if viewmodel.openMenu == Msgs.ItemMenu id then
                                                    [ Element.onLeft (itemMenu id choresDict viewmodel.cacheName viewmodel.cacheDesc)
                                                    ]
                                                        ++ activeItemStyle
                                                else if task.deleted then
                                                    deletedItemStyle
                                                else if task.completed then
                                                    completedItemStyle
                                                else
                                                    []
                                               )
                                        )
                                        (let
                                            leader =
                                                if itemstate == Msgs.Any then
                                                    String.fromChar '♦'
                                                else
                                                    String.fromChar
                                                        (if task.completed then
                                                            '✓'
                                                         else if task.deleted then
                                                            '✘'
                                                         else
                                                            '♦'
                                                        )
                                         in
                                            Element.text <| leader ++ " " ++ task.name
                                        )
                                )
                                choreList
                           )
                    )

        Models.Editing ->
            column
                [ height fill -- (px 80)
                , width <| (fill |> maximum (round ((toFloat viewmodel.dimensions.width) * 0.8)))

                --, paddingXY 10 0
                , centerX
                , centerY
                , Font.color (Colors.primary 1)
                , spacingXY 0 10
                ]
                [ el [ Font.color <| Colors.tertiary 1 ] (Element.text ("Editing current item id#: " ++ (Debug.toString currentChoreID)))
                , (Input.text [ Border.rounded 8, Background.color (Colors.tertiary 1), Element.focused [ Background.color (Element.rgb255 245 242 238), Border.shadow { offset = ( 0, 0 ), size = 1, blur = 8, color = (Colors.primary 0.5) } ], Input.focusedOnLoad, Element.htmlAttribute (Html.Attributes.id "task name input") ]
                    { onChange = \text -> Msgs.UpdView (Msgs.CacheName text)
                    , text = viewmodel.cacheName
                    , placeholder = Nothing
                    , label = Input.labelAbove [ Font.size 14 ] (Element.text "Add Task Name Here")
                    }
                  )
                , (Input.text [ Border.rounded 8, Background.color (Colors.tertiary 1), Element.focused [ Background.color (Element.rgb255 245 242 238), Border.shadow { offset = ( 0, 0 ), size = 1, blur = 8, color = (Colors.primary 0.5) } ] ]
                    { onChange = \text -> Msgs.UpdView (Msgs.CacheDesc text)
                    , text = viewmodel.cacheDesc
                    , placeholder = Nothing
                    , label = Input.labelAbove [ Font.size 14 ] (Element.text "Add Task Description Here")
                    }
                  )
                , Input.button
                    [ Border.rounded 8
                    , Background.color (Colors.primary 1.0)
                    , Font.color
                        (if String.isEmpty viewmodel.cacheName then
                            Colors.secondary 0.3
                         else
                            Element.rgb255 245 242 238
                        )
                    , Font.bold
                    , Font.size 24
                    , Font.variant Font.smallCaps
                    , padding 10
                    , Element.focused [ Background.color (Colors.primary 0.8) ]
                    ]
                    { onPress =
                        (if String.isEmpty viewmodel.cacheName then
                            Nothing
                         else
                            case currentChoreID of
                                Just x ->
                                    Just (Msgs.UpdData (Msgs.EditChore x viewmodel.cacheName viewmodel.cacheDesc))

                                _ ->
                                    Nothing
                        )
                    , label = Element.text "Submit"
                    }
                ]

        Models.AddNew ->
            el [] Element.none

        _ ->
            el [] (Element.none)


activeItemStyle =
    [ Font.bold ]


deletedItemStyle =
    [ Font.color (Element.rgba255 202 81 76 0.8) ]


completedItemStyle =
    [ Font.color (Element.rgba255 76 202 103 0.8) ]


itemMenu currentChoreID choreDict cacheName cacheDesc =
    column
        [ Element.behindContent <|
            Element.image
                [ Element.alpha 0.7
                , Element.scale 1
                , Element.moveUp 70
                , Element.moveRight 5
                ]
                { src = "src/Icons/arcmenuBckgrnd.svg"
                , description = "background of menu"
                }
        , Events.onMouseLeave <|
            Msgs.UpdView (Msgs.Menuize Msgs.None)
        ]
        [ Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.secondary 0.0)
            , Font.size 14
            , Element.moveUp (60 * (sin (degrees 100)))
            , Element.moveLeft (60 * (cos (degrees 100)) - 20)
            , padding 2
            , Element.focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , Element.scale 1
            , pointer
            , Events.onClick <| Msgs.UpdView Msgs.Editing

            --, Element.htmlAttribute <| stopPropagationOn "click" (Json.succeed ( (Msgs.UpdView (Msgs.Editing)), True ))
            ]
            { src = "src/Icons/editicon.svg", description = "icon to edit item" }
        , Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.primary 0.0)
            , Font.size 14
            , padding 2
            , Element.moveUp (46 + 60 * (sin (degrees 180)))
            , Element.moveLeft (60 * (sin (degrees 180)) - 16)
            , focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , pointer

            --, Events.onClick <| Msgs.UpdData (Msgs.CompleteChore currentChoreID)
            , Element.htmlAttribute <| stopPropagationOn "click" (Json.succeed ( (Msgs.UpdData (Msgs.CompleteChore currentChoreID)), True ))
            ]
            { src = "src/Icons/checkmark.svg", description = "icon to mark item complete" }
        , Element.image
            [ width (px 36)
            , height (px 36)
            , Border.rounded 36
            , Background.color (Colors.primary 0.0)
            , Font.size 14
            , padding 2
            , Element.moveUp (92 + 60 * (sin (degrees 260)))
            , Element.moveLeft (60 * cos (degrees 260) - 20)
            , focused []
            , mouseOver [ Background.color (Colors.secondary 0.9) ]
            , pointer

            --, Events.onClick <| Msgs.UpdData (Msgs.DeleteChore currentChoreID)
            , Element.htmlAttribute <| stopPropagationOn "click" (Json.succeed ( (Msgs.UpdData (Msgs.DeleteChore currentChoreID)), True ))
            ]
            { src = "src/Icons/trashicon.svg", description = "icon to delete item" }
        ]
