module Views exposing (..)

import Html
import Html.Attributes
import Html.Events
import Json.Decode as Json
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Events
import Dict exposing (..)
import Task
import Time


-- MY MODULES --

import Colors
import Chore exposing (Chore, ChoreDict)
import Msgs exposing (Flags)
import Fonts
import Models exposing (ViewModel, View)
import Buttons exposing (Btn, Button)
import DesktopView exposing (..)
import PhoneView exposing (..)
import TabletView exposing (..)


initialView : Flags -> ViewModel
initialView flags =
    { currentView = Models.ListItems Msgs.Any
    , focusedElement = ""
    , device = Element.classifyDevice flags
    , dimensions = flags
    , openMenu = Msgs.None
    , cacheName = ""
    , cacheDesc = ""
    }


update : Msgs.ViewMsg -> ViewModel -> ViewModel
update msg viewmdl =
    case msg of
        Msgs.CheckStats ->
            ({ viewmdl | currentView = Models.Statistics, openMenu = Msgs.None })

        Msgs.Menuize menustate ->
            ({ viewmdl | openMenu = menustate })

        Msgs.Itemize state ->
            ({ viewmdl
                | currentView = Models.ListItems state
                , openMenu = Msgs.None
                , cacheName = ""
                , cacheDesc = ""
             }
            )

        Msgs.Editing ->
            ({ viewmdl
                | currentView = Models.Editing
                , openMenu = Msgs.None
                , focusedElement = "task name input"
             }
            )

        Msgs.ClassifyDevice flags ->
            ({ viewmdl | device = Element.classifyDevice flags, dimensions = flags })

        Msgs.CacheName name ->
            ({ viewmdl | cacheName = name })

        Msgs.CacheDesc desc ->
            ({ viewmdl | cacheDesc = desc })

        _ ->
            (viewmdl)


view : ViewModel -> ChoreDict -> Maybe Int -> Html.Html Msgs.MainMsg
view model choresDict currChoreID =
    Element.layoutWith
        { options =
            [ focusStyle
                { borderColor = Just (Colors.primary 0.5)
                , backgroundColor = Just (Colors.tertiary 0.7)
                , shadow =
                    Just
                        { color = (Colors.primary 0.5)
                        , offset =
                            ( 0
                            , 0
                            )
                        , blur = 2
                        , size = 1
                        }
                }
            ]
        }
        ([ Background.color (Colors.tertiary 1.0)
         , width fill
         , height fill
         ]
            ++ (if model.device.class == Phone then
                    [ padding 20 ]
                else
                    []
               )
        )
    <|
        baseView model choresDict currChoreID


baseView : ViewModel -> ChoreDict -> Maybe Int -> Element Msgs.MainMsg
baseView model choresDict currChoreID =
    case model.device.class of
        Phone ->
            PhoneView.baseView model choresDict currChoreID

        Tablet ->
            TabletView.baseView model choresDict currChoreID

        _ ->
            DesktopView.baseView model choresDict currChoreID
