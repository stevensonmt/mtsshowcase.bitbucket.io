module Msgs exposing (..)

import Time
import Element exposing (..)


type ViewMsg
    = Itemize ItemState
    | Menuize MenuState
    | CheckStats
    | Editing
    | CacheName String
    | CacheDesc String
    | UpdateChore ChoreMsg
    | ClassifyDevice Flags


type ItemState
    = Any
    | Active
    | Complete
    | Deleted


type MenuState
    = MainMenu
    | ItemMenu Int
    | None


type ChoreMsg
    = EditChore Int String String
    | CompleteChore Int
    | DeleteChore Int
    | AddChore
    | NewChore Int
    | SweepAll
    | SweepComp
    | SweepDel


type MainMsg
    = UpdData ChoreMsg
    | UpdView ViewMsg
    | Rollback
    | GetTime Time.Posix
    | NoOp


type alias Flags =
    { width : Int, height : Int }
