module Models exposing (..)

import Chore exposing (..)
import Dict exposing (..)
import Time exposing (..)
import Element exposing (Device)
import Msgs


type alias Model =
    { tasks : ChoreDict
    , currentChoreID : Maybe Int
    , currentView : ViewModel
    , lastEdit : Edit
    , openMenu : Msgs.MenuState
    }


type Edit
    = Previous Model
    | NoEdit


getCurrentChore : Model -> Chore
getCurrentChore model =
    Maybe.withDefault Chore.new (Dict.get (Maybe.withDefault 0 model.currentChoreID) model.tasks)


type alias ViewModel =
    { currentView : View
    , focusedElement : String
    , device : Device
    , dimensions : Msgs.Flags
    , openMenu : Msgs.MenuState
    , cacheName : String
    , cacheDesc : String
    }


type View
    = AddNew
    | ListItems Msgs.ItemState
    | Editing
    | Statistics



{--type alias Flags =--}
{--{ width : Int, height : Int }--}
