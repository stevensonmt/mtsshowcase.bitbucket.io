module TabletView exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Fonts
import Colors
import Buttons
import DesktopView


baseView model choresDict currChoreID =
    row
        [ width <| fill
        , height <| fill
        , padding 20
        , spacing 10
        ]
        [ column
            [ width <| fillPortion 1
            , height <| fill
            , padding 10
            , spacing 6
            , Background.color (Colors.secondary 1)
            , Border.shadow
                { offset = ( 1, 1 )
                , size = 1.0
                , blur = 4
                , color = Colors.primary 0.5
                }
            , Font.color (Colors.primary 1)
            , Font.family [ Fonts.fontStack Fonts.PrimarySans ]
            ]
            [ el
                [ centerX
                , paddingEach { top = 20, bottom = 0, left = 0, right = 0 }
                , Font.size 32
                , Font.family [ Fonts.fontStack Fonts.Headline ]
                , Font.bold
                ]
                (Element.text "Listo p f")
            , mainMenu model.currentView (Buttons.buttons (Buttons.buttonList model.currentView) choresDict)
            ]
        , column
            [ width <| (fillPortion 5 |> minimum 400)
            , height <| fill
            , padding 20
            , spacing 10
            , Background.color (Colors.primary 1)
            , Border.shadow
                { offset = ( 1, 1 )
                , size = 1.0
                , blur = 4
                , color = Colors.primary 0.5
                }
            ]
            [ el [] (Element.text (Debug.toString model.device.class))
            , DesktopView.selectedView model choresDict currChoreID
            ]
        ]


mainMenu currentView buttonList =
    column
        [ spacing 16
        , centerY
        , height fill
        , padding 10
        , width fill
        , centerX
        , scrollbarY
        ]
        (List.map
            (\btn ->
                Input.button
                    [ width fill
                    , Border.rounded 28
                    ]
                    { onPress = Just btn.action
                    , label =
                        Element.row
                            [ width fill
                            , height <| px 28
                            , spacing 12
                            , centerY
                            , centerX
                            ]
                            [ Element.image
                                [ width <| px 16
                                , height <| px 16
                                , Border.rounded 16
                                , Element.scale 2
                                , centerX
                                ]
                                { src = "src/Icons/" ++ btn.src
                                , description = btn.description
                                }
                            ]
                    }
            )
            (buttonList)
        )
