module Chore exposing (..)

import Dict exposing (..)
import Time
import Msgs exposing (..)
import Task


type alias Chore =
    { name : String, description : String, completed : Bool, deleted : Bool, editing : Bool }


type alias ChoreDict =
    Dict Int Chore


new : Chore
new =
    { name = "", description = "", completed = False, deleted = False, editing = True }


delete : Chore -> Chore
delete task =
    { task | deleted = True }


complete : Chore -> Chore
complete task =
    { task | completed = True }


update : Msgs.ChoreMsg -> ChoreDict -> ChoreDict
update msg choreDict =
    case msg of
        CompleteChore id ->
            ((Dict.update id
                (\chore ->
                    case chore of
                        Just x ->
                            Just { x | completed = True }

                        Nothing ->
                            Nothing
                )
                choreDict
             )
            )

        DeleteChore id ->
            ((Dict.update id
                (\chore ->
                    case chore of
                        Just x ->
                            Just { x | deleted = True }

                        Nothing ->
                            Nothing
                )
                choreDict
             )
            )

        NewChore id ->
            Dict.insert id new choreDict

        EditChore id name desc ->
            let
                chore =
                    case Dict.get id choreDict of
                        Just a ->
                            { a | name = name, description = desc, editing = False }

                        _ ->
                            { name = name, description = desc, editing = False, deleted = False, completed = False }
            in
                Dict.insert id
                    chore
                    choreDict

        _ ->
            (choreDict)
