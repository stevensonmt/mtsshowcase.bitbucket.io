#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
use self::models::{User, NewUser};

pub mod schema;
pub mod models;

#[path = "mods/retrieve_users.rs"]
pub mod retrieve_users;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn create_user<'a>(conn: &PgConnection, oauth_id: &'a str, name: &'a str) -> User {
    use crate::schema::users;

    let new_user = NewUser { oauth_id: oauth_id, name: name};

    diesel::insert_into(users::table)
        .values(&new_user)
        .get_result(conn)
        .expect("Error saving new user")
}

pub fn retrieve_user<'a>(usr_id: i32) -> &'a User {
/*    use crate::schema::users::dsl::*;*/
    //let connection = establish_connection();
    /*let user = users.filter(id.eq(usr_id)).limit(1).load::<User>(&connection).expect("Error retrieving user");*/
    retrieve_users::retrieve_user(usr_id)
}
