table! {
    users (id) {
        id -> Int4,
        name -> Varchar,
        oauth_id -> Varchar,
        logged_in -> Bool,
    }
}
