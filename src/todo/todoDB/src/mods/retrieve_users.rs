extern crate diesel;

use crate::models::*;
use self::diesel::prelude::*;

pub fn retrieve_user<'a>(usr_id: i32) -> &'a User {
    use crate::schema::users::dsl::*;

    let connection = crate::establish_connection();
    let users = diesel::sql_query("SELECT * FROM users ORDER BY id").load(&connection).unwrap_or(vec![]);
    users.filter(|user| user.id == usr_id)


}
